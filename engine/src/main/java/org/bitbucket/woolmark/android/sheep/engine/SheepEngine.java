package org.bitbucket.woolmark.android.sheep.engine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.util.Random;

public class SheepEngine {

    public static final String KEY_SHEEP_NUMBER = "org.bitbucket.woolmark.sheep.number";

    private static final int INTERACTIVE_UPDATE_RATE_MS = 100;

    private static final int DEFAULT_WIDTH = 120;

    private static final int DEFAULT_HEIGHT = 120;

    private static final float GROUND_HEIGHT_RATIO = 0.9f;

    private static final int DELTA_X = 4;

    private static final int DELTA_Y = 3;

    private static final int JUMP_FRAME = 4;

    private static final int MSG_UPDATE_SHEEP = 1;

    private static Random sRandom = new Random();

    private int mScale = 1;

    private int mGroundHeight;

    private int mStrechLeg = 0;

    private int mCount;

    private int[][] mSheepFlock = new int[100][4];

    private int[][] mThumbnailSheepFlock = new int[1][4];

    private int mWidth;

    private int mHeight;

    private boolean mVisibility;

    private boolean mSheepStarting;

    private boolean mDestroyed;

    private boolean mThumbnail;

    private boolean mGrayScale;

    private int mSkyColor;

    private int mGroundColor;

    private Bitmap[] mSheepImage;

    private Bitmap mFenceImage;

    private Context mContext;

    private SheepListener mSheepListener;

    private final Handler mSheepUpdateHandler = new Handler() {
        @Override
        public void handleMessage(final Message message) {
            switch (message.what) {
                case MSG_UPDATE_SHEEP:
                    update();

                    if (mVisibility && !mThumbnail) {
                        long timeMs = System.currentTimeMillis();
                        long delayMs = INTERACTIVE_UPDATE_RATE_MS
                                - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                        mSheepUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SHEEP, delayMs);
                    }

                    break;
                default:
                    break;
            }
        }
    };

    public SheepEngine(final Context context) {
        this(context, null);
    }

    public SheepEngine(final Context context, final Bundle savedInstanceState) {

        mContext = context;

        if (savedInstanceState != null) {
            restoreState(savedInstanceState);
        }

        createResources();

        // Force initialize sheep positions
        mSheepFlock = createSheepFlock();
        mThumbnailSheepFlock = createThumbnailSheepFlock();

        mDestroyed = false;

        onCreate(savedInstanceState);
    }

    public Context getContext() {
        return mContext;
    }

    public int getSheepCount() {
        return mCount;
    }

    public int getScale() {
        return mScale;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public boolean isThumbnail() {
        return mThumbnail;
    }

    public boolean isGrayScale() {
        return mGrayScale;
    }

    public boolean isDestroyed() {
        return mDestroyed;
    }

    public void setThumbnail(final boolean thumbnail) {
        mThumbnail = thumbnail;
        updateTimer(mVisibility & !thumbnail);
    }

    public void setGrayScale(final boolean grayScale) {
        mGrayScale = grayScale;
    }

    public void setSize(final int width, final int height) {
        mWidth = width;
        mHeight = height;

        mScale = getPreferredScale(width, height);
        mGroundHeight = (int) ((float) (mFenceImage.getHeight() * mScale) * GROUND_HEIGHT_RATIO);

        mSheepFlock = createSheepFlock();
        mThumbnailSheepFlock = createThumbnailSheepFlock();

        onSizeChanged(width, height);
    }

    public void setSheepListener(final SheepListener listener) {
        mSheepListener = listener;
    }

    public void setVisibility(final boolean visibility) {
        mVisibility = visibility;
        updateTimer(visibility & !mThumbnail);

        onVisibilityChanged(visibility);
    }

    public void setSheepStarting(final boolean starting) {
        mSheepStarting = starting;
    }

    public void update() {

        updateSheepFlock(mSheepFlock);

        onUpdate();
        if (mSheepListener != null) {
            mSheepListener.onUpdate();
        }

    }

    public void draw(final Canvas canvas) {

        drawBackground(canvas, mGroundColor, mSkyColor);

        if (isThumbnail()) {
            mStrechLeg = 1;
            drawSheepFlock(canvas, mThumbnailSheepFlock);
        } else {
//            for (int i = 0; i < 200; i++) {
//                updateSheepFlock(mSheepFlock);
//                drawSheepFlock(canvas, mSheepFlock);
//            }
            drawSheepFlock(canvas, mSheepFlock);
        }

        onDraw(canvas);

    }

    public void destroy() {

        updateTimer(false);

        if (mSheepImage[0] != null) {
            mSheepImage[0].recycle();
            mSheepImage[0] = null;
        }
        if (mSheepImage[1] != null) {
            mSheepImage[1].recycle();
            mSheepImage[1] = null;
        }
        if (mFenceImage != null) {
            mFenceImage.recycle();
            mFenceImage = null;
        }

        mDestroyed = true;

        onDestroy();
    }

    public void saveState(final Bundle outState) {
        outState.putInt(KEY_SHEEP_NUMBER, mCount);
    }

    public void restoreState(final Bundle state) {
        mCount = state.getInt(KEY_SHEEP_NUMBER, 0);
    }

    protected int getPreferredScale(final int width, final int height) {

        int scale;

        if (width < height) {
            scale = width / DEFAULT_WIDTH;
        } else {
            scale = height / DEFAULT_HEIGHT;
        }

        if (scale < 1) {
            scale = 1;
        }

        return scale;
    }

    protected synchronized int[][] createSheepFlock() {

        int[][] sheepFlock = new int[100][4];

        // set the sheep position
        for (int i = 0; i < sheepFlock.length; i++) {
            removeSheep(sheepFlock, i);
        }
        addSheep(sheepFlock, 0);

        // Initialize preview sheep position
        return sheepFlock;
    }

    protected synchronized int[][] createThumbnailSheepFlock() {

        int[][] sheepFlock = new int[1][4];

        sheepFlock[0][0] = (mWidth + mSheepImage[0].getWidth() * mScale) / 2
                + DELTA_X * 4 * mScale;
        sheepFlock[0][1] = mHeight - (mGroundHeight / 2);

        return sheepFlock;
    }

    protected synchronized void updateSheepFlock(final int[][] sheepFlock) {

        // add a sheep per a frame
        if (mSheepStarting) {
            for (int i = 0; i < sheepFlock.length; i++) {
                if (sheepFlock[i][1] < 0) {
                    addSheep(sheepFlock, i);
                    break;
                }
            }
        }

        // run the sheep
        for (int i = 0; i < sheepFlock.length; i++) {
            int[] sheep = sheepFlock[i];

            if (sheep[1] < 0) {
                continue;
            }

            // run
            sheep[0] -= DELTA_X * mScale;

            // jump
//            if (sheep[2] < 0 && sheep[3] <= sheep[0]
//                                        && sheep[0] < sheep[3] + mScale * DELTA_X)  {
            if (sheep[3] - DELTA_X * mScale <= sheep[0] && sheep[0] < sheep[3])  {
//            if (sheep[3] - DELTA_X * 2 * mScale <= sheep[0] && sheep[0] < sheep[3] - DELTA_X * mScale)  {
                sheep[2] = 0;
            }
            if (sheep[2] >= 0) {
                if (sheep[2] < JUMP_FRAME) {
                    sheep[1] -= DELTA_Y * mScale;
                } else if (sheep[2] < JUMP_FRAME * 2) {
                    sheep[1] += DELTA_Y * mScale;
                } else {
                    sheep[2] = Integer.MIN_VALUE;
                }

                sheep[2] += 1;
            }

            if (sheep[2] == JUMP_FRAME) {
                mCount += 1;
                if (mSheepListener != null) {
                    mSheepListener.onJumped();
                }
            }

            // remove a go away sheep
            if (sheep[0] < -1 * mSheepImage[0].getWidth() * mScale) {
                removeSheep(sheepFlock, i);

                int j;
                for (j = 0; j < sheepFlock.length; j++) {
                    if (sheepFlock[j][1] >= 0) {
                        break;
                    }
                }

                if (j >= sheepFlock.length) {
                    addSheep(sheepFlock, 0);
                }
            }

        }

        mStrechLeg = 1 - mStrechLeg;

    }

    protected synchronized void drawBackground(
            final Canvas canvas, final int groundColor, final int skyColor) {

        Paint paint = new Paint();
        if (isGrayScale()) {
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0);
            paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        }

        // draw sky
        paint.setColor(skyColor);
        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), paint);

        // draw ground
        paint.setColor(groundColor);
        canvas.drawRect(0, canvas.getHeight() - mGroundHeight,
                canvas.getWidth(), canvas.getHeight(), paint);

        int scale = getScale();

        // draw the fence
        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale);
        matrix.postTranslate(
                (canvas.getWidth() - mFenceImage.getWidth() * scale) / 2,
                canvas.getHeight() - mFenceImage.getHeight() * scale);
        canvas.drawBitmap(mFenceImage, matrix, paint);

//        paint.setColor(Color.rgb(255, 0, 0));
//        paint.setStrokeWidth(10);
//        canvas.drawLine(calcJumpX(0), 0, calcJumpX(getHeight()), getHeight(), paint);
    }

    protected synchronized void drawSheepFlock(final Canvas canvas, final int[][] sheepFlock) {

        Paint paint = new Paint();
        if (isGrayScale()) {
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0);
            paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        }

        int scale = getScale();

        // draw sheep
        for (int[] sheep : sheepFlock) {
            Matrix matrix = new Matrix();
            matrix.setScale(scale, scale);

            if (sheep[1] < 0) {
                continue;
            }
            matrix.postTranslate(sheep[0], sheep[1]);
            if (sheep[2] >= 0) {
                canvas.drawBitmap(mSheepImage[1], matrix, paint);
            } else {
                canvas.drawBitmap(mSheepImage[mStrechLeg], matrix, paint);
            }
        }

    }

    protected void onCreate(final Bundle savedInstanceState) {

    }

    protected void onUpdate() {

    }

    protected void onDraw(final Canvas canvas) {

    }

    protected void onVisibilityChanged(final boolean visibility) {

    }

    protected void onSizeChanged(final int width, final int height) {

    }

    protected void onDestroy() {

    }

    private void createResources() {

        Context context = getContext();

        // Init colors
        mSkyColor = context.getResources().getColor(R.color.sky);
        mGroundColor = context.getResources().getColor(R.color.ground);

        // Init decode option
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            options.inScaled = false;
        }

        // load the sheep images
        mSheepImage = new Bitmap[2];
        mSheepImage[0] = BitmapFactory.decodeResource(
                context.getResources(), R.drawable.sheep00, options);
        mSheepImage[1] = BitmapFactory.decodeResource(
                context.getResources(), R.drawable.sheep01, options);

        // load a fence image
        mFenceImage = BitmapFactory.decodeResource(
                context.getResources(), R.drawable.fence, options);

    }

    private void updateTimer(final boolean needRestart) {

        mSheepUpdateHandler.removeMessages(MSG_UPDATE_SHEEP);
        if (needRestart) {
            mSheepUpdateHandler.sendEmptyMessage(MSG_UPDATE_SHEEP);
        }

    }

    private void addSheep(int[][] sheepFlock, final int index) {

        if (mHeight > 0) {
            sheepFlock[index][0] = mWidth + mSheepImage[0].getWidth() * mScale;
            sheepFlock[index][1] = mHeight - mGroundHeight + sRandom.nextInt(
                    mGroundHeight - mSheepImage[0].getHeight() * mScale);
//            sheepFlock[index][1] = mHeight - mGroundHeight + 300;
            sheepFlock[index][2] = Integer.MIN_VALUE;
            sheepFlock[index][3] = calcJumpX(sheepFlock[index][1]);
        }

    }

    private void removeSheep(int[][] sheepFlock, final int index) {

        sheepFlock[index][0] = 0;
        sheepFlock[index][1] = -1;
        sheepFlock[index][2] = 0;
        sheepFlock[index][3] = 0;

    }

    private int calcJumpX(final int y) {
        // y = -1 * fence.height / fence.width * (x - (width - fence.width) / 2) + height

        int w = mWidth;
        int h = mHeight;
        int fw = mFenceImage.getWidth() * mScale;
        int fh = (mFenceImage.getHeight()) * mScale;

        return -1 * (y - h) * fw / fh + (w - fw) / 2;

    }

    public static interface SheepListener {

        void onUpdate();

        void onJumped();

    }

}
