package org.bitbucket.woolmark.android.sheep.one;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import org.bitbucket.woolmark.android.sheep.engine.SheepEngine;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SheepView view = (SheepView) findViewById(R.id.canvas);
        final SheepEngine engine = view.getSheepEngine();

        final TextView counter = (TextView) findViewById(R.id.counter);
        counter.setText(getString(R.string.counter, engine.getSheepCount()));

        view.getSheepEngine().setSheepListener(new SheepEngine.SheepListener() {
            @Override
            public void onUpdate() {
                view.invalidate();
            }

            @Override
            public void onJumped() {
                counter.setText(getString(R.string.counter, engine.getSheepCount()));
            }
        });

    }

}
