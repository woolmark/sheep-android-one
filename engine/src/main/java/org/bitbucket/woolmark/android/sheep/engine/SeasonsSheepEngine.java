package org.bitbucket.woolmark.android.sheep.engine;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import java.util.Calendar;
import java.util.Random;

public class SeasonsSheepEngine extends SheepEngine {

    private static final String ACTION_TIME_CHANGED = "org.bitbucket.woolmark.android.sheep.engine.action.TIME_SET";

    private BroadcastReceiver mDateChangeReceiver;

    private SeasonEngine mSeasonEngine;

    public SeasonsSheepEngine(Context context) {
        this(context, null);
    }

    public SeasonsSheepEngine(Context context, final Bundle savedInstanceState) {
        super(context, savedInstanceState);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSeasonEngine = getSeasonEngine(savedInstanceState);

        mDateChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mSeasonEngine = getSeasonEngine(savedInstanceState);

                setOneDayAfterAlarm();
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        intentFilter.addAction(Intent.ACTION_DATE_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        intentFilter.addAction(ACTION_TIME_CHANGED);
        getContext().registerReceiver(mDateChangeReceiver, intentFilter);

        setOneDayAfterAlarm();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mDateChangeReceiver != null) {
            getContext().unregisterReceiver(mDateChangeReceiver);
        }
    }

    @Override
    protected void onSizeChanged(final int width, final int height) {
        super.onSizeChanged(width, height);

        if (mSeasonEngine != null) {
            mSeasonEngine.onSizeChanged(width, height);
        }
    }

    @Override
    protected void onUpdate() {
        super.onUpdate();

        if (mSeasonEngine != null) {
            mSeasonEngine.onUpdate();
        }
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        if (mSeasonEngine != null) {
            mSeasonEngine.onDraw(canvas);
        }
    }

    @Override
    protected void drawBackground(
            final Canvas canvas, final int groundColor, final int skyColor) {

        if (mSeasonEngine != null) {
            mSeasonEngine.drawBackground(canvas, groundColor, skyColor);
        } else {
            super.drawBackground(canvas, groundColor, skyColor);
        }

    }

    private SeasonEngine getSeasonEngine(final Bundle savedInstanceState) {

        SeasonEngine engine = null;

        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        if (month == Calendar.JANUARY && calendar.get(Calendar.DAY_OF_MONTH) < 7) {
            engine = new NewYearSeasonEngine();
        } else if (month == Calendar.DECEMBER && (day >= 17 && day <= 25)) {
            engine = new ChristmasSeasonEngine();
        } else if (month == Calendar.JANUARY || month == Calendar.DECEMBER) {
            engine = new WinterSeasonEngine();
        }

        if (engine != null) {
            engine.onCreate(savedInstanceState);
        }

        return engine;

    }

    private void setOneDayAfterAlarm() {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DAY_OF_MONTH, 1);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 0,
                new Intent(ACTION_TIME_CHANGED), PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);

        alarmManager.cancel(pendingIntent);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT){
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }else{
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }

    }

    private abstract class SeasonEngine {

        protected void onCreate(final Bundle savedInstanceState) {
        }

        protected void onSizeChanged(final int width, final int height) {
        }

        protected void onUpdate() {
        }

        protected void onDraw(final Canvas canvas) {
        }

        protected void drawBackground(
                final Canvas canvas, final int groundColor, final int skyColor) {
            SeasonsSheepEngine.super.drawBackground(canvas, groundColor, skyColor);
        }

        protected void drawMessage(
                final Canvas canvas, final String message,
                final int messageColor, final int strokeColor,
                final Typeface typeFace,
                final Paint paint) {

            paint.setAntiAlias(true);
            paint.setTypeface(typeFace);
            paint.setTextSize(getContext().getResources().getDimension(R.dimen.message_font_size));

            paint.setColor(strokeColor);
            paint.setStrokeWidth(getContext().getResources().getDimension(R.dimen.message_stroke));
            paint.setStyle(Paint.Style.STROKE);
            drawMessage(canvas, message, paint);

            paint.setColor(messageColor);
            paint.setStrokeWidth(0.0f);
            paint.setStyle(Paint.Style.FILL);
            drawMessage(canvas, message, paint);

        }

        protected void drawMessage(final Canvas canvas, final String message, final Paint paint) {

            int height = (int) (getContext().getResources().getDimension(R.dimen.message_margin_top));

            for (String msg : message.split("\n")) {
                canvas.drawText(msg,
                        (canvas.getWidth() - (int) paint.measureText(msg)) / 2,
                        height + paint.getTextSize(), paint);

                height += (int) (paint.getTextSize()
                        + getContext().getResources().getDimension(R.dimen.message_line_divider));
            }

        }

    }

    private class WinterSeasonEngine extends SeasonEngine {

        private static final int SNOW_SIZE = 1;

        private Random mRandom = new Random();

        private int mFrameCount;

        private int[][] mSnows = new int[100][3];

        @Override
        protected void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            mSnows = new int[100][3];
            for (int[] snow : mSnows) {
                snow[0] = Integer.MIN_VALUE;
                snow[1] = Integer.MIN_VALUE;
            }

        }

        @Override
        protected void onSizeChanged(final int width, final int height) {
            super.onSizeChanged(width, height);

            for (int[] snow : mSnows) {
                snow[0] = Integer.MIN_VALUE;
                snow[1] = Integer.MIN_VALUE;
            }

        }

        @Override
        protected void onUpdate() {
            super.onUpdate();

            mFrameCount++;
            if (mFrameCount % 10 == 0) {
                addSnow();
            }

            if (mSnows != null) {
                for (int[] snow : mSnows) {
                    snow[1] += snow[2];

                    if (snow[1] > getHeight()) {
                        snow[1] = Integer.MIN_VALUE;
                    }
                }
            }
        }

        @Override
        protected void onDraw(final Canvas canvas) {
            super.onDraw(canvas);

            if (mSnows != null) {
                Paint paint = new Paint();
                paint.setColor(Color.WHITE);

                int scale = getScale();
                for (int[] snow : mSnows) {
                    canvas.drawCircle(snow[0], snow[1], SNOW_SIZE * scale, paint);
                }
            }

        }

        @Override
        protected void drawBackground(
                final Canvas canvas, final int groundColor, final int skyColor) {

            super.drawBackground(canvas,
                    getContext().getResources().getColor(R.color.winter_ground),
                    skyColor);

        }

        private void addSnow() {

            if (getWidth() > 0) {
                int scale = getScale();
                int addedSnowNumber = 0;
                int newSnowNumber = mRandom.nextInt(getWidth() / 100);
                if (newSnowNumber <= 0) {
                    newSnowNumber = 1;
                }

                for (int[] snow : mSnows) {
                    if (snow[1] < 0) {
                        snow[0] = mRandom.nextInt(getWidth());
                        snow[1] = 0;
                        snow[2] = 3 + (int) (mRandom.nextFloat() * scale * 3);

                        addedSnowNumber++;
                        if (addedSnowNumber >= newSnowNumber) {
                            break;
                        }
                    }
                }
            }

        }

    }

    private class NewYearSeasonEngine extends WinterSeasonEngine {

        private static final String NEW_YEAR_FONT_FILE_PATH = "COCKTAIL.TTF";

        private Typeface mMessageTypeFace;

        @Override
        protected void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            mMessageTypeFace = Typeface.createFromAsset(
                    getContext().getAssets(), NEW_YEAR_FONT_FILE_PATH);

        }

        @Override
        protected void drawBackground(
                final Canvas canvas, final int groundColor, final int skyColor) {
            super.drawBackground(canvas, groundColor, skyColor);

            drawMessage(canvas, getContext().getString(R.string.happy_new_year),
                    skyColor, Color.WHITE, mMessageTypeFace, new Paint());

        }

    }

    private class ChristmasSeasonEngine extends WinterSeasonEngine {

        @Override
        protected void drawBackground(
                final Canvas canvas, final int groundColor, final int skyColor) {
            super.drawBackground(canvas, groundColor, skyColor);

            drawMessage(canvas, getContext().getString(R.string.merry_christmas),
                    Color.RED, getContext().getResources().getColor(R.color.christmas_stroke),
                    null, new Paint());

        }

    }

}
