package org.bitbucket.woolmark.sheep.android.wallpaper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.WallpaperManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class SheepWallpaperChooserActivity extends Activity {

    public static final String RESOURCE_PREFIX = "wallpaper_";

    private static final int DILAOG_APPLY = 0x0999;

    private static final String TAG = "sheep.wallpaper";

    private int mSelectedIndex;

    private List<Integer> mDrawableValues;

    private List<String> mDrawableNames;

    private ImageView mBackground;

    private Button mTitleView;

    private ImageButton mPrevButton;

    private ImageButton mNextButton;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wallpaper);

        mBackground = (ImageView) findViewById(android.R.id.background);

        mTitleView = (Button) findViewById(android.R.id.text1);
        mTitleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApplyClicked();
            }
        });

        mPrevButton = (ImageButton) findViewById(R.id.prev_button);
        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPrevClicked();
            }
        });

        mNextButton = (ImageButton) findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNextClicked();
            }
        });

        mDrawableValues = new ArrayList<Integer>();
        mDrawableNames = new ArrayList<String>();
        for (Field field : R.drawable.class.getDeclaredFields()) {
            String name = field.getName();
            if (name.startsWith(RESOURCE_PREFIX)) {
                try {
                    mDrawableValues.add((int) field.get(null));
                    mDrawableNames.add(name.substring(RESOURCE_PREFIX.length())
                            .replace('_', ' ').toUpperCase());
                } catch (IllegalAccessException e) {
                    Log.e(TAG, "cannot get wallpaper resource value:" + e);
                }
            }
        }

        setPreviewWallpaper(0);

    }

    @Override
    protected Dialog onCreateDialog(final int id, final Bundle args) {

        Dialog dialog;

        if (id == DILAOG_APPLY) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(
                    R.string.confirm_change_wallpaper, args.getString(Intent.EXTRA_TITLE)));
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    applyWallpaper(args.getInt(Intent.EXTRA_UID));
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            dialog = builder.create();
        } else {
            dialog = super.onCreateDialog(id, args);
        }

        return dialog;

    }

    protected void onPrevClicked() {

        if (mSelectedIndex > 0) {
            mSelectedIndex--;
        }

        setPreviewWallpaper(mSelectedIndex);

    }

    protected void onNextClicked() {

        if (mSelectedIndex < mDrawableValues.size() - 1) {
            mSelectedIndex++;
        }

        setPreviewWallpaper(mSelectedIndex);

    }

    protected void onApplyClicked() {

        Bundle args = new Bundle();
        args.putString(Intent.EXTRA_TITLE, mDrawableNames.get(mSelectedIndex));
        args.putInt(Intent.EXTRA_UID, mDrawableValues.get(mSelectedIndex));

        showDialog(DILAOG_APPLY, args);

    }

    protected void setPreviewWallpaper(final int index) {

        mBackground.setImageResource(mDrawableValues.get(index));
        mTitleView.setText(mDrawableNames.get(index));

    }

    protected void applyWallpaper(final int resId) {

        new WallpaperApplyTask().execute(resId);

    }

    private class WallpaperApplyTask extends AsyncTask<Integer, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Boolean doInBackground(Integer... params) {

            boolean result;
            int resId = params[0];

            WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());

            try {
                wallpaperManager.setResource(resId);
                result = true;
            } catch (IOException e) {
                Log.e(TAG, "cannot set wallpaper resource:" + e);
                result = false;
            }

            return result;

        }

        @Override
        protected void onPostExecute(final Boolean result) {
            super.onPostExecute(result);

            if (result) {
                finish();
            }
        }

    }
}
