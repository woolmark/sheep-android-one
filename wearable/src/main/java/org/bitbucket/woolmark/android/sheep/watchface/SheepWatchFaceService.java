package org.bitbucket.woolmark.android.sheep.watchface;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.view.SurfaceHolder;
import android.view.WindowInsets;

import org.bitbucket.woolmark.android.sheep.engine.SheepEngine;
import org.bitbucket.woolmark.android.sheep.one.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SheepWatchFaceService extends CanvasWatchFaceService {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public CanvasWatchFaceService.Engine onCreateEngine() {
        return new SheepWatchfaceEngine();
    }

    private class SheepWatchfaceEngine
            extends CanvasWatchFaceService.Engine
            implements SheepEngine.SheepListener {

        private static final int WATCH_TYPE_ROUND = 0;

        private static final int WATCH_TYPE_SQUARE = 1;

        private static final String FONT_FILE_PATH = "lcddot_tr.ttf";

        private int mWatchType = WATCH_TYPE_ROUND;

        private Typeface mDateTimeFace;

        private SheepEngine mEngine;

        public void onCreate(final SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);

            setWatchFaceStyle(new WatchFaceStyle.Builder(SheepWatchFaceService.this)
                    .setCardPeekMode(WatchFaceStyle.PEEK_MODE_SHORT)
                    .setBackgroundVisibility(WatchFaceStyle
                            .BACKGROUND_VISIBILITY_INTERRUPTIVE)
                    .setShowSystemUiTime(false)
                    .build());

            mDateTimeFace = Typeface.createFromAsset(
                    SheepWatchFaceService.this.getAssets(), FONT_FILE_PATH);

            mEngine = new SheepEngine(SheepWatchFaceService.this);
            mEngine.setSheepListener(this);

            Rect frame = surfaceHolder.getSurfaceFrame();
            mEngine.setSize(frame.width(), frame.height());
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);

            mEngine.setSize(width, height);
            invalidate();
        }

        @Override
        public void onApplyWindowInsets(WindowInsets insets) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                super.onApplyWindowInsets(insets);
            }

            if (insets.isRound()) {
                mWatchType = WATCH_TYPE_ROUND;
            } else {
                mWatchType = WATCH_TYPE_SQUARE;
            }
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();

            invalidate();
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            mEngine.setVisibility(visible);
            if (visible) {
                invalidate();
            }
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);

            if (inAmbientMode) {
                mEngine.setGrayScale(true);
                mEngine.setThumbnail(true);
            } else {
                mEngine.setGrayScale(false);
                mEngine.setThumbnail(false);
            }

            invalidate();
        }

        @Override
        public void onDraw(final Canvas canvas, final Rect bounds) {
            super.onDraw(canvas, bounds);

            mEngine.draw(canvas);
            drawDateTime(canvas);
        }

        @Override
        public void onDestroy() {
            mEngine.destroy();
        }

        @Override
        public void onUpdate() {
            invalidate();
        }

        @Override
        public void onJumped() {

        }

        private void drawDateTime(final Canvas canvas) {

            Calendar now = Calendar.getInstance();
            String time = (new SimpleDateFormat(getString(R.string.time_format), Locale.ENGLISH)).format(now.getTime())
                    + (now.get(Calendar.HOUR_OF_DAY) < 12 ? getString(R.string.am) : getString(R.string.pm));
            String date = (new SimpleDateFormat(
                    getString(R.string.date_format),
                    Locale.ENGLISH)).format(now.getTime());
            // String time = DateFormat.format(getString(R.string.time_format), now).toString();
            // String date = DateFormat.format(getString(R.string.date_format), now).toString();

            Paint paint = new Paint();
            if (isInAmbientMode()) {
                paint.setColor(Color.WHITE);
            } else {
                paint.setColor(Color.BLACK);
            }
            paint.setAntiAlias(false);
            paint.setTypeface(mDateTimeFace);

            switch (mWatchType) {
            case WATCH_TYPE_ROUND:
                paint.setTextSize(getResources().getDimension(R.dimen.time_font_size));
                drawTime(canvas, now,
                        (canvas.getWidth() - (int) paint.measureText(time)) / 2,
                        (int) (getResources().getDimension(R.dimen.watchface_round_time_margin_top)
                                + paint.getTextSize()), paint);

                paint.setTextSize(getResources().getDimension(R.dimen.date_font_size));
                canvas.drawText(date,
                        (canvas.getWidth() - (int) paint.measureText(date)) / 2,
                        getResources().getDimension(R.dimen.watchface_round_time_margin_top)
                                + getResources().getDimension(R.dimen.time_font_size)
                                + getResources().getDimension(R.dimen.watchface_round_date_margin_top)
                                + paint.getTextSize(), paint);
                break;
            case WATCH_TYPE_SQUARE:
                paint.setTextSize(getResources().getDimension(R.dimen.time_font_size));
                drawTime(canvas, now,
                        (int) (canvas.getWidth() - (int) paint.measureText(time)
                                - getResources().getDimension(R.dimen.watchface_rect_time_margin_right)),
                        (int) (getResources().getDimension(R.dimen.watchface_rect_time_margin_top)
                                + paint.getTextSize()),
                        paint);

                paint.setTextSize(getResources().getDimension(R.dimen.date_font_size));
                canvas.drawText(date,
                        canvas.getWidth() - (int) paint.measureText(date)
                                - getResources().getDimension(R.dimen.watchface_rect_date_margin_right),
                        getResources().getDimension(R.dimen.watchface_rect_date_margin_top)
                                + getResources().getDimension(R.dimen.time_font_size)
                                + getResources().getDimension(R.dimen.watchface_rect_time_margin_top)
                                + paint.getTextSize(),
                        paint);
                break;
            default:
                break;
            }

        }

        private void drawTime(final Canvas canvas, final Calendar now,
                              final int x, final int y, final Paint paint) {

            String time = (new SimpleDateFormat(getString(R.string.time_format), Locale.ENGLISH)).format(now.getTime())
                    + (now.get(Calendar.HOUR_OF_DAY) < 12 ? getString(R.string.am) : getString(R.string.pm));

            int colonIndex = time.indexOf(':');
            if (colonIndex < 0 || isInAmbientMode()) {
                canvas.drawText(time, x, y, paint);
            } else {
                int hourMeasure = (int) paint.measureText(time, 0, colonIndex);
                canvas.drawText(time, 0, colonIndex, x, y, paint);

                if (now.get(Calendar.MILLISECOND) < 500) {
                    canvas.drawText(":", x + hourMeasure, y, paint);
                }

                canvas.drawText(time, colonIndex + 1, time.length(),
                        x + hourMeasure + paint.measureText(":"), y, paint);
            }

        }

    }

}