package org.bitbucket.woolmark.android.sheep.appwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.RemoteViews;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SheepAppWidgetProvider extends AppWidgetProvider {

    public static final String ACTION_UPDATE_ALIVE = "org.bitbucket.woolmark.android.sheep.appwidget.action.ALIVE_UPDATE";

    public static final String RESOURCE_PREFIX = "sheep_anim";

    private static List<Integer> sResourceList;

    private static Map<Integer, SheepWidgetEngine> sEngineMap = null;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        super.onReceive(context, intent);

        if (ACTION_UPDATE_ALIVE.equals(intent.getAction()) && sEngineMap != null) {
            int widgetId = intent.getIntExtra(Intent.EXTRA_UID, 0);
            SheepWidgetEngine engine = sEngineMap.get(widgetId);
            if (engine == null) {
                engine = new SheepWidgetEngine(context,
                        AppWidgetManager.getInstance(context), widgetId);
                engine.onCreate(null);

                sEngineMap.put(widgetId, engine);
            }

            engine.onToggleAlive();
        }
    }

    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager,
                         final int[] appWidgetIds) {

        createResourceList();

        if (sEngineMap == null) {
            sEngineMap = new HashMap<>();
        }

        for (int id : appWidgetIds) {
            if (!sEngineMap.containsKey(id)) {
                SheepWidgetEngine engine = new SheepWidgetEngine(context, appWidgetManager, id);
                engine.onCreate(null);

                sEngineMap.put(id, engine);
            }
        }

    }

    @Override
    public void onDeleted(final Context context, final int[] appWidgetIds) {

        if (sEngineMap != null) {
            for (int id : appWidgetIds) {
                if (sEngineMap.containsKey(id)) {
                    sEngineMap.get(id).onDestroy();
                    sEngineMap.remove(id);
                }
            }
        }

    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);

        createResourceList();

        if (sEngineMap == null) {
            sEngineMap = new HashMap<>();
        }

    }

    @Override
    public void onDisabled(Context context) {

        if (sEngineMap != null) {
            for (int id : sEngineMap.keySet()) {
                sEngineMap.get(id).onDestroy();
            }
        }

        sEngineMap = null;

    }

    private void createResourceList() {

        if (sResourceList == null) {

            List<String> resourceNames = new ArrayList<>();
            for (Field field : R.drawable.class.getDeclaredFields()) {
                String name = field.getName();
                if (name.startsWith(RESOURCE_PREFIX)) {
                    resourceNames.add(name);
                }
            }
            Collections.sort(resourceNames, new Comparator<String>() {
                @Override
                public int compare(String lhs, String rhs) {
                    return Integer.parseInt(lhs.substring(RESOURCE_PREFIX.length()))
                            - Integer.parseInt(rhs.substring(RESOURCE_PREFIX.length()));
                }
            });

            sResourceList = new ArrayList<>();
            for (String name : resourceNames) {
                try {
                    sResourceList.add((int) R.drawable.class.getDeclaredField(name).get(null));
                } catch (IllegalAccessException e) {
                } catch (NoSuchFieldException e) {
                }
            }

        }

    }

    private class SheepWidgetEngine {

        private static final int INTERACTIVE_UPDATE_RATE_MS = 100;

        private static final int MSG_UPDATE_SHEEP = 1;

        private int mWidgetId;

        private boolean mAlive;

        private int mDrawResourceIndex;

        private Context mContext;

        private AppWidgetManager mAppWidgetManager;

        private final Handler mSheepUpdateHandler = new Handler() {
            @Override
            public void handleMessage(final Message message) {
                switch (message.what) {
                    case MSG_UPDATE_SHEEP:
                        onUpdate();
                        onDraw();

                        if (mAlive) {
                            long timeMs = System.currentTimeMillis();
                            long delayMs = INTERACTIVE_UPDATE_RATE_MS
                                    - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                            mSheepUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SHEEP, delayMs);
                        }

                        break;
                    default:
                        break;
                }
            }
        };

        public SheepWidgetEngine(Context context, final AppWidgetManager appWidgetManager,
                                 final int widgetId) {

            mWidgetId = widgetId;

            mContext = context;
            mAppWidgetManager = appWidgetManager;

        }

        public void onCreate(final Bundle savedInstanceState) {

            RemoteViews views = new RemoteViews(
                    mContext.getPackageName(), R.layout.appwidget_main);

            Intent intent = new Intent(mContext, SheepAppWidgetProvider.class);
            intent.setAction(ACTION_UPDATE_ALIVE);
            intent.putExtra(Intent.EXTRA_UID, mWidgetId);

            views.setOnClickPendingIntent(android.R.id.background, PendingIntent.getBroadcast(
                    mContext, mWidgetId, intent, PendingIntent.FLAG_UPDATE_CURRENT));

            mAppWidgetManager.updateAppWidget(mWidgetId, views);

            mAlive = true;
            updateTimer(true);
        }

        public void onDestroy() {
            mAlive = false;

            updateTimer(false);
        }

        protected void onToggleAlive() {
            mAlive = !mAlive;
            updateTimer(mAlive);

            mDrawResourceIndex = 7;
            onDraw();
        }

        public void onUpdate() {
            update();
        }

        public void onDraw() {

            RemoteViews views = new RemoteViews(
                    mContext.getPackageName(), R.layout.appwidget_main);
            draw(views);

        }

        protected synchronized void update() {

            mDrawResourceIndex++;
            if (mDrawResourceIndex >= sResourceList.size()) {
                mDrawResourceIndex = 0;
            }

        }

        protected synchronized void draw(final RemoteViews views) {

            views.setImageViewResource(android.R.id.background,
                    sResourceList.get(mDrawResourceIndex));
            mAppWidgetManager.updateAppWidget(mWidgetId, views);

        }

        private void updateTimer(final boolean needRestart) {

            mSheepUpdateHandler.removeMessages(MSG_UPDATE_SHEEP);
            if (needRestart) {
                mSheepUpdateHandler.sendEmptyMessage(MSG_UPDATE_SHEEP);
            }

        }

    }

}
