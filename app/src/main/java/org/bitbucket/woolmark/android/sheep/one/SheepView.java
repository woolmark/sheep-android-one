package org.bitbucket.woolmark.android.sheep.one;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import org.bitbucket.woolmark.android.sheep.engine.SeasonsSheepEngine;
import org.bitbucket.woolmark.android.sheep.engine.SheepEngine;

public class SheepView extends View {

    private SheepEngine mEngine;

    public SheepView(Context context) {
        this(context, null);
    }

    public SheepView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SheepView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        Bundle state = new Bundle();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (prefs != null) {
            state.putInt(SheepEngine.KEY_SHEEP_NUMBER,
                    PreferenceManager.getDefaultSharedPreferences(getContext())
                            .getInt(SheepEngine.KEY_SHEEP_NUMBER, 0));
        }

        mEngine = new SeasonsSheepEngine(context, state);
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mEngine.setSheepStarting(true);
                return true;
            case MotionEvent.ACTION_UP:
                mEngine.setSheepStarting(false);
                return true;
            default:
                break;
        }

        return super.onTouchEvent(event);
    }

    public SheepEngine getSheepEngine() {
        return mEngine;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (!mEngine.isDestroyed()) {
            mEngine.setSize(getWidth(), getHeight());
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (mEngine == null || mEngine.isDestroyed()) {
            Bundle state = new Bundle();

            state.putInt(SheepEngine.KEY_SHEEP_NUMBER,
                    PreferenceManager.getDefaultSharedPreferences(getContext())
                            .getInt(SheepEngine.KEY_SHEEP_NUMBER, 0));

            mEngine = new SeasonsSheepEngine(getContext(), state);
        }
        mEngine.setSize(getWidth(), getHeight());

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        Bundle state = new Bundle();
        mEngine.saveState(state);

        PreferenceManager.getDefaultSharedPreferences(getContext()).edit()
                .putInt(SheepEngine.KEY_SHEEP_NUMBER, state.getInt(SheepEngine.KEY_SHEEP_NUMBER, 0))
                .commit();

        mEngine.destroy();
    }

    @Override
    protected void onSizeChanged (int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        if (mEngine != null) {
            mEngine.setSize(w, h);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mEngine != null) {
            mEngine.draw(canvas);
        }
    }

    @Override
    protected void onWindowVisibilityChanged(final int visibility) {
        super.onWindowVisibilityChanged(visibility);

        if (visibility == View.VISIBLE) {
            mEngine.setVisibility(true);
            invalidate();
        } else {
            mEngine.setVisibility(false);
        }

    }

}
