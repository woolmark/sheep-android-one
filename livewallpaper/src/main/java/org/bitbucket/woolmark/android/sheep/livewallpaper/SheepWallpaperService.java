package org.bitbucket.woolmark.android.sheep.livewallpaper;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.service.wallpaper.WallpaperService;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import org.bitbucket.woolmark.android.sheep.engine.SeasonsSheepEngine;
import org.bitbucket.woolmark.android.sheep.engine.SheepEngine;

@TargetApi(Build.VERSION_CODES.ECLAIR_MR1)
public class SheepWallpaperService extends WallpaperService {

	@Override
	public Engine onCreateEngine() {
		return new SheepWallpaperEngine();
	}

	@TargetApi(Build.VERSION_CODES.ECLAIR_MR1)
    private class SheepWallpaperEngine extends WallpaperService.Engine
            implements SheepEngine.SheepListener {

		private SheepEngine mEngine;

        public void onCreate(final SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);

            mEngine = new SeasonsSheepEngine(SheepWallpaperService.this);
            mEngine.setSheepListener(this);
        }

	    @Override
	    public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	        super.onSurfaceChanged(holder, format, width, height);

            mEngine.setSize(width, height);
	    }

	    @Override
	    public void onVisibilityChanged(boolean visible) {
	    	super.onVisibilityChanged(visible);

            mEngine.setVisibility(visible);
            if (visible) {
                onUpdate();
            }
	    }

        @Override
        public void onTouchEvent(final MotionEvent event) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mEngine.setSheepStarting(true);
                    return;
                case MotionEvent.ACTION_UP:
                    mEngine.setSheepStarting(false);
                    return;
            }
            super.onTouchEvent(event);
        }

        @Override
        public void onDestroy() {
            mEngine.destroy();
        }

        @Override
        public void onUpdate() {

            Canvas canvas = getSurfaceHolder().lockCanvas();
            if (canvas != null) {
                mEngine.draw(canvas);
                drawSheepCount(canvas);

                try {
                    getSurfaceHolder().unlockCanvasAndPost(canvas);
                } catch (IllegalArgumentException e) {
                    // when the device is rotated, canvas is destroyed and is unable to unlock
                    // that exception should be ignored.
                }
            }

        }

        @Override
        public void onJumped() {

        }

        private void drawSheepCount(final Canvas canvas) {

            Paint paint = new Paint();

            paint.setColor(Color.BLACK);
            paint.setTextSize(getResources().getDimension(R.dimen.counter_font_size));
            canvas.drawText(
                    getString(R.string.counter, mEngine.getSheepCount()),
                    getResources().getDimension(R.dimen.wallpaper_counter_margin_left),
                    getResources().getDimension(R.dimen.wallpaper_counter_margin_top)
                            + paint.getTextSize() + getStatusBarHeight(), paint);

        }

	    private int getStatusBarHeight() {
	        int result = 0;
	        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
	        if (resourceId > 0) {
	            result = getResources().getDimensionPixelSize(resourceId);
	        }
	        return result;
	    }

    }

}
