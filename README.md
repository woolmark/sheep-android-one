# Sheep for Android One
Sheep for Android One is an implementation [sheep] for Android.

# Requirements

 - Android Studio 2.0
 - Android Device or Emulator
 - Android Wearable Watch (Optional)

# How to set up and build

 1. Launch Android Studio
 2. Select `Open an existing Android Studio project`
 3. Choose the clone directory
 4. Click [Run] - [Run 'app']

# Sheep components

 - Activity
 - Live Wallpaper
 - Wallpaper Chooser
 - HOME and Keyguard Widget
 - Watch face for Android Wear

# Get it on Google Play
[![Get it on Google Play][get_it_logo]](https://play.google.com/store/apps/details?id=org.bitbucket.woolmark.android.sheep.one)

# License
[WTFPL] 

[sheep]: https://woolmark.bitbucket.org/ "Sheep"
[WTFPL]: http://www.wtfpl.net "WTFPL"

[get_it_logo]: https://developer.android.com/images/brand/en_generic_rgb_wo_45.png
[get_it]: https://play.google.com/store/apps/details?id=org.bitbucket.woolmark.sheep.android.one
